unit uSelectVersionForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.StdCtrls, FMX.ListBox, FMX.Controls.Presentation;

type
  TfrmVersionSelector = class(TForm)
    lblVersionTitle: TLabel;
    cmbbxVersions: TComboBox;
    btnOK: TButton;
    Layout1: TLayout;
  private
  public
    class function SelectVersion(
      const AStyleBook: TStyleBook;
      const AVersions: TStrings): String;
  end;

implementation

{$R *.fmx}

class function TfrmVersionSelector.SelectVersion(
  const AStyleBook: TStyleBook;
  const AVersions: TStrings): String;
var
  Form: TfrmVersionSelector;
begin
  Application.CreateForm(TfrmVersionSelector, Form);
  try
    Form.StyleBook := AStyleBook;

    Form.cmbbxVersions.Items.Assign(AVersions);
    Form.cmbbxVersions.ItemIndex := Form.cmbbxVersions.Count - 1;

    Form.ShowModal;

    var Index := Form.cmbbxVersions.ItemIndex;
    if Index < 0 then
      Index := 0;

    Result := AVersions[Index];
  finally
    Form.Free;
  end;
end;

end.
